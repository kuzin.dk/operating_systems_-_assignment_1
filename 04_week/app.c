#include <stdio.h>
#include <malloc.h>
#include "io.h"

struct node {
    int value;
    struct node *prev;
};
struct node* append(struct node* latestNode, int counter) {
    struct node* newNode = (struct node*) malloc(sizeof(struct node));

    newNode->prev = latestNode;
    newNode->value = counter;

    return newNode;
}
struct node* pop(struct node* latestNode) {
    struct node* tmpNode = latestNode->prev;

    free(latestNode);

    return tmpNode;
}
void write(int value) {
    if (value > 9) {
        write(value / 10);
        write_char('0' + (value % 10));
    } else {
        write_char('0' + value);
    }
}
void print(struct node* latestNode) {
    if (latestNode->prev != NULL) {
        print(latestNode->prev);
        write_char(',');
        write(latestNode->value);
    } else {
        write(latestNode->value);
    }
    free(latestNode);
}
void program(struct node* latestNode, int counter) {
    char c;

    syscall_read(&c,1);

    if(c == 'a') {
        latestNode = append(latestNode, counter);
        counter++;
        program(latestNode, counter);
    } else if (c == 'b') {
        counter++;
        program(latestNode, counter);
    } else if (c == 'c') {
        latestNode = pop(latestNode);
        counter++;
        program(latestNode, counter);
    } else {
        if(latestNode == NULL) {
            int size = 7;
            int i = 0;
            char blank[7] = "<blank>";
            while(i < size) {
                write_char(blank[i]);
                i++;
            }
        } else {
            print(latestNode);
        }
        write_char('\n');
    }
}
int main() {
    program(NULL, 0);
    return 0;
}


