#include <stdio.h>
#include <malloc.h>
#include "io.h"

void write(int value) {
    if (value > 9) {
        write(value / 10);
        write_char('0' + (value % 10));
    } else {
        write_char('0' + value);
    }
}
int main() {
    int i = 0;
    while(i < 1000) {
        write(i);
        write_char('\n');
        i++;
    }
    return 0;
}
