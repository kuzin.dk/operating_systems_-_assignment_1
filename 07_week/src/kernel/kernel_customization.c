/* Copyright (c) 1997-2017, FenixOS Developers
   All Rights Reserved.

   This file is subject to the terms and conditions defined in
   file 'LICENSE', which is part of this source code package.
 */

/*! \file kernel_customization.c This file holds definitions
  for the kernel that you can change and alter. */

#include <stdint.h>
#include <instruction_wrappers.h>
#include <sysdefines.h>

#include "kernel.h"

int create_process(uint32_t program_index) {
    int t = MAX_THREADS;
    for (int i = 0; i < MAX_THREADS; i++) {
        if (threads[i].active == 0) {
            t = i;
            break;
        }
    }
    if(t == MAX_THREADS) {
        kprints("Max threads");
        return 0;
    }
    threads[t].eip = executable_table[program_index];
    threads[t].active = 1;
    int p = MAX_PROCESSES;
    for (int i = 0; i < MAX_PROCESSES; i++) {
        if (processes[i].active == 0) {
            p = i;
            break;
        }
    }
    if(p == MAX_PROCESSES) {
        kprints("Max processes");
        return 0;
    }
    processes[p].number_of_threads = 1;
    processes[p].active = 1;
    threads[t].process = &processes[p];

    return 1;

}
int create_thread() {
    int t = MAX_THREADS;
    for (int i = 0; i < MAX_THREADS; i++) {
        if (threads[i].active == 0) {
            t = i;
            break;
        }
    }
    if(t == MAX_THREADS) {
        kprints("Max threads");
        return 0;
    }

    threads[t].process = current_thread->process;
    threads[t].active = 1;
    threads[t].esp = current_thread->esi;
    threads[t].eip = current_thread->edi;

    return 1;

}


void kernel_late_init(void) {
    /* Set up the first thread. For now we do not set up a process. That is
      for you to do later. */

    if (!create_process(0)) {
        asm("HLT");
    }
    /* Go to user space. */
    go_to_user_space();
}


void handle_system_call(void) {
    switch (current_thread->eax) {
        case SYSCALL_VERSION: {
            current_thread->eax = 0x00010000;
            break;
        }
        case SYSCALL_PRINTS: {
            kprints((char *) current_thread->edi);
            current_thread->eax = ALL_OK;
            break;
        }
        case SYSCALL_CREATEPROCESS: {
            if (create_process(current_thread->edi)) {
                current_thread->eax = ALL_OK;
            } else {
                current_thread->eax = ERROR;
            }
            break;
        }
        case SYSCALL_CREATETHREAD: {
            if (create_thread()) {
                current_thread->eax = ALL_OK;
            } else {
                current_thread->eax = ERROR;
            }
            break;
        }
        case SYSCALL_YIELD: {
            uint32_t c = ((uint32_t)current_thread - (uint32_t)threads) / sizeof(struct thread);

            uint32_t t = c;
            for (uint32_t i = c+1; i != c; i++) {
                if (i == MAX_THREADS) {
                    i = 0;
                }
                if (threads[i].active == 1) {
                    t = i;
                    break;
                }
            }
            current_thread = &threads[t];
            break;
        }
        case SYSCALL_TERMINATE:
            current_thread->active = 0;
            current_thread->process->number_of_threads--;

            if (current_thread->process->number_of_threads == 0) {
                current_thread->process->active = 0;
            }
            uint32_t c = ((uint32_t)current_thread - (uint32_t)threads) / sizeof(struct thread);

            uint32_t t = c;
            for (uint32_t i = c+1; i != c; i++) {
                if (i == MAX_THREADS) {
                    i = 0;
                }
                if (threads[i].active == 1) {
                    t = i;
                    break;
                }
            }
            current_thread = &threads[t];
            kprints("Reverting to thread ");
            kprinthex((uint32_t) t);
            kprints("\n");
            break;
        default: {
            /* Unrecognized system call. Not good. */
            current_thread->eax = ERROR_ILLEGAL_SYSCALL;
        }
    }

    go_to_user_space();
}

/* Add more definitions here if you need. */
