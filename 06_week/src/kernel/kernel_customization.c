/* Copyright (c) 1997-2017, FenixOS Developers
   All Rights Reserved.

   This file is subject to the terms and conditions defined in
   file 'LICENSE', which is part of this source code package.
 */

/*! \file kernel_customization.c This file holds definitions
  for the kernel that you can change and alter. */

#include <stdint.h>
#include <instruction_wrappers.h>
#include <sysdefines.h>

#include "kernel.h"

void kernel_late_init(void) {
    /* Set up the first thread. For now we do not set up a process. That is
      for you to do later. */
    threads[0].active = 1;
    threads[0].eip = executable_table[0];

    /* Go to user space. */
    go_to_user_space();
}

void handle_system_call(void) {
    switch (current_thread->eax) {
        case SYSCALL_VERSION: {
            current_thread->eax = 0x00010000;
            break;
        }
        case SYSCALL_PRINTS: {
            kprints((char *) current_thread->edi);
            current_thread->eax = ALL_OK;
            break;
        }
        case SYSCALL_CREATEPROCESS: {
            int t = -1;
            for (int i = 0; i < MAX_THREADS; i++) {
                if (threads[i].active == 0) {
                    t = i;
                    break;
                }
            }
            if(t == -1) {
                current_thread->eax = ERROR;
                break;
            }
            threads[t].eip = executable_table[current_thread->edi];
            threads[t].active = 1;
            int p = -1;
            for (int i = 0; i < MAX_PROCESSES; i++) {
                if (processes[i].active == 0) {
                    p = i;
                    break;
                }
            }
            if(p == -1) {
                current_thread->eax = ERROR;
                break;
            }
            processes[p].number_of_threads = 1;
            processes[p].active = 1;
            threads[t].process = &processes[p];

            current_thread->eax = ALL_OK;
            break;
        }
        case SYSCALL_TERMINATE: {
            current_thread->active = 0;
            current_thread->process->number_of_threads--;

            if (current_thread->process->number_of_threads == 0) {
                current_thread->process->active = 0;
            }
            uint32_t current_thread_id = ((uint32_t) current_thread - (uint32_t) threads) / sizeof(struct thread);

            uint32_t t = current_thread_id;
            for (uint32_t i = current_thread_id + 1; i != current_thread_id; i++) {
                if (i == MAX_THREADS) {
                    i = 0;
                }
                if (threads[i].active == 1) {
                    t = i;
                    break;
                }
            }
            current_thread = &threads[t];
            kprints("Reverting to thread ");
            kprinthex((uint32_t) t);
            kprints("\n");
            break;
        }
        case SYSCALL_YIELD: {
            uint32_t current_thread_id = ((uint32_t)current_thread - (uint32_t)threads) / sizeof(struct thread);

            uint32_t t = current_thread_id;
            for (uint32_t i = current_thread_id + 1; i != current_thread_id; i++) {
                if (i == MAX_THREADS) {
                    i = 0;
                }
                if (threads[i].active == 1) {
                    t = i;
                    break;
                }
            }
            current_thread = &threads[t];
            break;
        }
        default: {
            /* Unrecognized system call. Not good. */
            current_thread->eax = ERROR_ILLEGAL_SYSCALL;
        }
    }

    go_to_user_space();
}

/* Add more definitions here if you need. */
